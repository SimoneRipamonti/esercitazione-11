package clientserver;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server {
	/**
	 * Porta su cui sta in ascolto il server
	 */
	private final static int PORT = 29999;

	/**
	 * Metodo che avvia il server
	 * 
	 * @throws IOException
	 */
	public void startServer() throws IOException {
		/**
		 * Scelgo un executor che eseguir� i thread e quale politica dargli
		 */
		ExecutorService executor = Executors.newCachedThreadPool();
		/**
		 * Crea un server socket in attesa sulla porta PORT
		 */
		ServerSocket serverSocket = new ServerSocket(PORT);
		/**
		 * Mi metto in attesa di connessioni
		 */
		System.out.println("Server avviato, in attesa di connessione");
		while (true) {
			Socket socket = serverSocket.accept();
			/**
			 * Metto il socket nel pool di esecuzione
			 */
			executor.submit(new ServerSocketView(socket));
			System.out.println("Client connesso");
		}

	}

	public static void main(String[] args) throws IOException {
		Server echoServer = new Server();
		echoServer.startServer();
	}
}
