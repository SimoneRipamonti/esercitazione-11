package clientserver;

import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

/**
 * Implementa un thread
 * 
 * @author Simone
 *
 */
public class ServerSocketView implements Runnable {
	private final Socket socket;

	public ServerSocketView(Socket socket) {
		this.socket = socket;
	}

	@Override
	public void run() {
		try {
			/**
			 * Scanner utilizzato per leggere dal socket. Posso usare anche un
			 * buffered reader
			 */
			
			Scanner socketIn = new Scanner(socket.getInputStream());
			/**
			 * PrintWriter utilizzato per scrivere sul socket
			 */
			PrintWriter socketOut = new PrintWriter(socket.getOutputStream());

			boolean end = false;

			while (!end) {
				/**
				 * finch� il client non manda un carattere resto su questa riga,
				 * attendo finche non ne ricevo uno
				 */
				String line = socketIn.nextLine();

				System.out.println("SERVER: received the string " + line);
				if (line.equals("quit")) {
					end = true;
				} else {
					socketOut
							.println("Well done client, the server received the message: "
									+ line);
					/**
					 * Uso flush per inviare i messaggi, poich� printwriter di
					 * default bufferizza
					 */
					socketOut.flush();
				}
				/**
				 * se il client scrive mentre il server fa altro, il messaggio
				 * viene bufferizzato e dato al server nella prossima esecuzione
				 * di socketIn.nextLine()
				 */
			}

		} catch (Exception e) {

		}
	}
}
