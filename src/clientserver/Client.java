package clientserver;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class Client {
	private final static int PORT = 29999;
	private final static String IP = "127.0.0.1";

	/**
	 * Metodo che fa partire il client
	 * 
	 * @throws IOException
	 * @throws UnknownHostException
	 */
	public void startClient() throws UnknownHostException, IOException {

		/**
		 * Creo la connessione al server
		 */
		Socket socket = new Socket(IP, PORT);
		/**
		 * Scanner per leggere il server
		 */
		Scanner socketIn = new Scanner(socket.getInputStream());
		/**
		 * PrintWriter per scrivere al server
		 */
		PrintWriter socketOut = new PrintWriter(socket.getOutputStream());
		/**
		 * Scanner per leggere la riga di comando
		 */
		Scanner stdin = new Scanner(System.in);
		
		while(true){
			String inputLine = stdin.nextLine();
			
			socketOut.println(inputLine);
			socketOut.flush();
			
			String serverMessage = socketIn.nextLine();
			System.out.println(serverMessage);
		}
	}
	
	public static void main (String[] args) throws UnknownHostException, IOException{
		Client client = new Client();
		client.startClient();
	}
}
